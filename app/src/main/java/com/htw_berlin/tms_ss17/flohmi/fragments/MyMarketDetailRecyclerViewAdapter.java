/*
 * This file is part of FlohmiApp.
 *
 * All rights reserved.
 */
package com.htw_berlin.tms_ss17.flohmi.fragments;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.TextView;

import com.htw_berlin.tms_ss17.flohmi.model.Market;
import com.htw_berlin.tms_ss17.flohmi.R;
import com.htw_berlin.tms_ss17.flohmi.fragments.MarketDetailFragment.OnListFragmentInteractionListener;

/**
 * MarketDetail Adapter
 *
 * @author Semih Kasap
 * @author Max Scheibke
 * @author Yannick Vahldieck
 */
class MyMarketDetailRecyclerViewAdapter extends RecyclerView.Adapter<MyMarketDetailRecyclerViewAdapter.ViewHolder> {

    private final Market mValue;
    private final OnListFragmentInteractionListener mListener;
    private final Context mContext;

    MyMarketDetailRecyclerViewAdapter(Market item, OnListFragmentInteractionListener listener, Context context) {
        mValue = item;
        mListener = listener;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_marketdetail, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValue;
        holder.mTitleView.setText(mValue.getPlace());
        holder.mDistrictView.setText(mValue.getDistrict());
        holder.mDaysView.setText(mValue.getDays());
        holder.mHoursView.setText(mValue.getHours());
        holder.mOperatorView.setText(mValue.getOperator());
        holder.mEmailView.setText(mValue.getEmail());
        holder.mWwwView.setText(mValue.getWww());
        holder.mNoticesView.setText(mValue.getNotices());

        if (mValue.getFavorite()) {
            holder.mFavoriteButton.setChecked(true);
        }

        holder.mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = ((FragmentActivity) mContext).getSupportFragmentManager();
                fm.popBackStackImmediate();
            }
        });

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        final TextView mTitleView;
        final TextView mDistrictView;
        final TextView mDaysView;
        final TextView mHoursView;
        final TextView mOperatorView;
        final TextView mEmailView;
        final TextView mWwwView;
        final TextView mNoticesView;
        final CheckBox mFavoriteButton;
        final ImageButton mBackButton;
        Market mItem;

        ViewHolder(View view) {
            super(view);
            mView = view;
            mTitleView = (TextView) view.findViewById(R.id.marketdetail_title_content);
            mDistrictView = (TextView) view.findViewById(R.id.marketdetail_district_content);
            mDaysView = (TextView) view.findViewById(R.id.marketdetail_days_content);
            mHoursView = (TextView) view.findViewById(R.id.marketdetail_hours_content);
            mOperatorView = (TextView) view.findViewById(R.id.marketdetail_operator_content);
            mEmailView = (TextView) view.findViewById(R.id.marketdetail_email_content);
            mWwwView = (TextView) view.findViewById(R.id.marketdetail_www_content);
            mNoticesView = (TextView) view.findViewById(R.id.marketdetail_notices_content);
            mFavoriteButton = (CheckBox) view.findViewById(R.id.favoritebutton);
            mBackButton = (ImageButton) view.findViewById(R.id.backbutton);
        }
    }
}
