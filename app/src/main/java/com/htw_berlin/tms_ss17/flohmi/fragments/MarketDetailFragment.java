/*
 * This file is part of FlohmiApp.
 *
 * All rights reserved.
 */
package com.htw_berlin.tms_ss17.flohmi.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.htw_berlin.tms_ss17.flohmi.model.Market;
import com.htw_berlin.tms_ss17.flohmi.R;
import com.htw_berlin.tms_ss17.flohmi.utils.DataManager;

/**
 * MarketDetail Fragment
 *
 * @author Semih Kasap
 * @author Max Scheibke
 * @author Yannick Vahldieck
 */
public class MarketDetailFragment extends Fragment {

    private static final String ARG_MARKETTITLE = "market-title";
    private OnListFragmentInteractionListener mListener;
    private Market mMarket;

    public MarketDetailFragment() {
    }

    public static MarketDetailFragment newInstance(String marketTitle) {
        MarketDetailFragment fragment = new MarketDetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_MARKETTITLE, marketTitle);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            String mMarketTitle = getArguments().getString(ARG_MARKETTITLE);
            mMarket = getMarketFromTitle(mMarketTitle);
        }
    }

    public static Market getMarketFromTitle(String marketTitle) {
        for (Market m : DataManager.shared().getAllMarkets()) {
            if (m.getPlace().equals(marketTitle))
                return m;
        }
        Log.e(MarketDetailFragment.class.getName(), "No equivalent market could be found for the clicket map marker!");
        return null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_marketdetail_list, container, false);

        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.nav_details));

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.setAdapter(new MyMarketDetailRecyclerViewAdapter(mMarket, mListener, context));
        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(Market item);
    }
}
