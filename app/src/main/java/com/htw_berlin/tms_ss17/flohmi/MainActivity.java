/*
 * This file is part of FlohmiApp.
 *
 * All rights reserved.
 */
package com.htw_berlin.tms_ss17.flohmi;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.htw_berlin.tms_ss17.flohmi.fragments.DisclaimerFragment;
import com.htw_berlin.tms_ss17.flohmi.fragments.FavoritesFragment;
import com.htw_berlin.tms_ss17.flohmi.fragments.MapsFragment;
import com.htw_berlin.tms_ss17.flohmi.fragments.MarketDetailFragment;
import com.htw_berlin.tms_ss17.flohmi.fragments.NearMarketsFragment;
import com.htw_berlin.tms_ss17.flohmi.model.Market;
import com.htw_berlin.tms_ss17.flohmi.persistence.MarketsDataSource;
import com.htw_berlin.tms_ss17.flohmi.utils.DataManager;
import com.htw_berlin.tms_ss17.flohmi.utils.LocationUtils;
import com.htw_berlin.tms_ss17.flohmi.utils.NetworkManager;
import com.htw_berlin.tms_ss17.flohmi.utils.NetworkManagerCallback;
import com.htw_berlin.tms_ss17.flohmi.utils.UsageManager;

import java.util.List;

/**
 * Main activity which is handling all fragments and is also doing following steps:
 * 1. Returning all markets from offline database (SQLite)
 * 2. Requesting OpenData API for new markets
 * 3. Checking location permissions
 * 4. Get periodic location updates
 *
 * @author Semih Kasap
 * @author Max Scheibke
 * @author Yannick Vahldieck
 */
public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, FavoritesFragment.OnListFragmentInteractionListener, NearMarketsFragment.OnListFragmentInteractionListener, LocationListener, MarketDetailFragment.OnListFragmentInteractionListener, NetworkManagerCallback {

    private static final int GET_LOCATION_FINE = 1;

    /** Access persisted markets in the database. */
    private MarketsDataSource dataSource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("APP_LIFE_CYCLE", "MainActivity: protected void onCreate(Bundle savedInstanceState)");

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // Instantiate the access to the database.
        dataSource = new MarketsDataSource(this);
        dataSource.open();
        List<Market> markets = dataSource.getAllMarkets();
        dataSource.close();
        DataManager.shared().addMarket(markets);

        // INFO: Data from network API should be requested after offline data is loaded yet.
        NetworkManager networkManager = new NetworkManager(this);
        networkManager.execute(getString(R.string.market_api));

        // Instantiate location manager
        LocationManager locationManager = LocationUtils.shared().getLocationManager(this);
        if (locationManager == null)
            locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        // Request location related permission if not granted yet
        if (!UsageManager.checkPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            UsageManager.requestPermission(this, Manifest.permission.ACCESS_FINE_LOCATION, GET_LOCATION_FINE);
        }

        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction().replace(R.id.content_frame, new MapsFragment()).commit();
    }

    @Override
    public void onBackPressed() {
        Log.d("APP_LIFE_CYCLE", "MainActivity: onBackPressed()");

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Log.d("APP_LIFE_CYCLE", "MainActivity: public boolean onNavigationItemSelected(@NonNull MenuItem item)");

        // Handle navigation view item clicks here.
        int id = item.getItemId();
        FragmentManager fm = getSupportFragmentManager();

        if (id == R.id.nav_map) {
            fm.beginTransaction().replace(R.id.content_frame, new MapsFragment(), "visible_fragment").addToBackStack(null).commit();

        } else if (id == R.id.nav_favorites) {
            fm.beginTransaction().replace(R.id.content_frame, new FavoritesFragment(), "visible_fragment").addToBackStack(null).commit();

        } else if (id == R.id.nav_nearMarkets) {
            fm.beginTransaction().replace(R.id.content_frame, new NearMarketsFragment(), "visible_fragment").addToBackStack(null).commit();

        } else if (id == R.id.nav_disclaimer) {
            fm.beginTransaction().replace(R.id.content_frame, new DisclaimerFragment(), "visible_fragment").addToBackStack(null).commit();
        }

        // Highlight the selected item has been done by NavigationView
        item.setChecked(true);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d("APP_LIFE_CYCLE", "MainActivity: public void onLocationChanged(Location location)");
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.d("APP_LIFE_CYCLE", "MainActivity: public void onStatusChanged(String provider, int status, Bundle extras)");
        Log.d("APP_LIFE_CYCLE", "String provider = " + provider);
        Log.d("APP_LIFE_CYCLE", "int status = " + status);

        Toast.makeText(getBaseContext(), getString(R.string.gps_activated),
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.d("APP_LIFE_CYCLE", "MainActivity: public void onProviderEnabled(String provider)");
        Log.d("APP_LIFE_CYCLE", "String provider = " + provider);
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.d("APP_LIFE_CYCLE", "MainActivity: public void onProviderDisabled(String provider)");
        Log.d("APP_LIFE_CYCLE", "String provider = " + provider);

        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
        Toast.makeText(getBaseContext(), getString(R.string.gps_not_activated),
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onListFragmentInteraction(Market item) {
        Log.d("APP_LIFE_CYCLE", "MainActivity: public void onListFragmentInteraction(Market item)");
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction().replace(R.id.content_frame, MarketDetailFragment.newInstance(item.getPlace())).addToBackStack("nearmarkets").commit();
    }

    /**
     * Toggle favorite button's state.
     * @param view Relevant view
     */
    public void toggleFavoriteState(View view) {
        TextView mTitle = (TextView) view.getRootView().findViewById(R.id.marketdetail_title_content);
        Market mItem = MarketDetailFragment.getMarketFromTitle((String) mTitle.getText());
        if (mItem == null) {
            Log.e(this.getClass().getName(), getString(R.string.detail_view_current_market_not_determ));
            return;
        }

        if (mItem.getFavorite()) {
            mItem.setFavorite(false);
        } else mItem.setFavorite(true);

        dataSource.open();
        dataSource.updateMarket(mItem);
        dataSource.close();
    }

    @Override
    public void onNetworkRequestResult() {
        Log.d("APP_LIFE_CYCLE", "MainActivity: public void onNetworkRequestResult()");

        List<Fragment> fragments;
        MapsFragment mapsFragment;

        FragmentManager fm = getSupportFragmentManager();
        fragments = fm.getFragments();

        if (fragments == null)
            return;

        for (Fragment f: fragments) {
            if (f instanceof MapsFragment) {
                mapsFragment = (MapsFragment) f;
                mapsFragment.showAllMarketMarkers();
                break;
            }
        }

        // Update database
        dataSource.open();
        for (Market m: DataManager.shared().getAllMarkets())
            dataSource.updateMarket(m);
        dataSource.close();
    }
}
