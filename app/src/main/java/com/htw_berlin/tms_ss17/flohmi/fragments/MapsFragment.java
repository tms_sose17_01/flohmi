/*
 * This file is part of FlohmiApp.
 *
 * All rights reserved.
 */
package com.htw_berlin.tms_ss17.flohmi.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.htw_berlin.tms_ss17.flohmi.model.Market;
import com.htw_berlin.tms_ss17.flohmi.R;
import com.htw_berlin.tms_ss17.flohmi.utils.UsageManager;
import com.htw_berlin.tms_ss17.flohmi.utils.DataManager;
import com.htw_berlin.tms_ss17.flohmi.utils.LocationUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Maps Fragment
 *
 * @author Semih Kasap
 * @author Max Scheibke
 * @author Yannick Vahldieck
 */
public class MapsFragment extends SupportMapFragment implements OnMapReadyCallback, LocationListener {

    /**
     * Self-chosen ID for location requests.
     */
    private static final int GET_LOCATION_FINE = 1;

    /**
     * Google maps API
     */
    private GoogleMap mMap;

    /**
     * Markets in the view
     */
    private List<Marker> markers;

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Log.d("APP_LIFE_CYCLE", "MapsFragment: public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle)");

        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.map));

        NavigationView navigationView = (NavigationView) getActivity().findViewById(R.id.nav_view);
        Menu menu = navigationView.getMenu();
        MenuItem item = menu.findItem(R.id.nav_map);
        item.setChecked(true);

        final Activity activity = getActivity();

        FrameLayout mapView = (FrameLayout) super.onCreateView(layoutInflater, viewGroup, bundle);

        // Instanziate three radio buttons within a radiogroup
        final RadioButton[] radioButtons = new RadioButton[3];
        final RadioGroup radioGroup = new RadioGroup(activity);
        radioGroup.setOrientation(RadioGroup.HORIZONTAL);

        String[] buttonLabels = new String[]{
                activity.getString(R.string.filter_open),
                activity.getString(R.string.filter_today),
                activity.getString(R.string.filter_all)
        };

        for (int i = 0; i < radioButtons.length || i < buttonLabels.length; i++) {
            radioButtons[i] = new RadioButton(activity);
            radioGroup.addView(radioButtons[i]);
            radioButtons[i].setText(buttonLabels[i]);
            radioButtons[i].setId(i + 100);
            if (i == 2)
                radioButtons[i].setChecked(true);
        }

        radioButtons[0].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showOpenedMarketMarkers();
            }
        });

        radioButtons[1].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTodayMarketMarkers();
            }
        });

        radioButtons[2].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAllMarketMarkers();
            }
        });

        if (mapView != null)
        mapView.addView(radioGroup);

        return mapView;
    }

    @Override
    public void onCreate(Bundle bundle) {
        Log.d("APP_LIFE_CYCLE", "MapsFragment: public void onCreate(Bundle bundle)");
        super.onCreate(bundle);

        // Get current context
        Context context = getActivity();

        // Instantiate the maps system
        getMapAsync(this);

        // Request permissions if they are not granted yet.
        if (!UsageManager.checkPermission(context, Manifest.permission.ACCESS_FINE_LOCATION))
            UsageManager.requestPermission(context, Manifest.permission.ACCESS_FINE_LOCATION, GET_LOCATION_FINE);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d("APP_LIFE_CYCLE", "MapsFragment: public void onMapReady(GoogleMap googleMap)");

        // Hold google's map system locally in fragment
        mMap = googleMap;

        // Do not continue if there are no location permissions.
        if (UsageManager.checkPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Enabling the my-location layer to determine the current device position.
            mMap.setMyLocationEnabled(true);

            // Get last known location of the GPS sensor and show it on the map
            Location initialLocation;
            initialLocation = LocationUtils.shared().getLastKnownLocation(getActivity());
            if (initialLocation != null) {
                Log.d(this.getClass().getName(), getString(R.string.cam_last_known));
                moveCamera(initialLocation);
            }

        } else {
            Log.d(this.getClass().getName(), getString(R.string.perm_granted));
        }

        for (Market m : DataManager.shared().getAllMarkets())
            addMarker(m);

        // Popup window for displaying additional info when clicking an annotation.
        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            @Override
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                ViewGroup viewGroup = (ViewGroup) getView();
                View markerInfo = getActivity().getLayoutInflater().inflate(R.layout.infowindow_marker, viewGroup, false);
                TextView marketTitle = (TextView) markerInfo.findViewById(R.id.marketTitle);
                TextView marketSnippet = (TextView) markerInfo.findViewById(R.id.marketSnippet);

                marketTitle.setText(marker.getTitle());
                marketSnippet.setText(marker.getSnippet());

                return markerInfo;
            }
        });

        // Click on Popup window to open details
        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                if (marker == null) {
                    Log.e(this.getClass().getName(), getString(R.string.no_marker_by_market));
                    return;
                }

                Market market = getMarketFromSnippet(marker);
                if (market == null) {
                    Log.e(this.getClass().getName(), getString(R.string.no_clicked_marked_determined));
                    return;
                }

                FragmentManager fm = getActivity().getSupportFragmentManager();
                Fragment fragment = MarketDetailFragment.newInstance(market.getPlace());
                fm.beginTransaction().replace(R.id.content_frame, fragment).addToBackStack("map").commit();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d("APP_LIFE_CYCLE", "MapsFragment: public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)");
        Log.d("APP_LIFE_CYCLE", "int requestCode = " + requestCode);
        for (String s: permissions)
            Log.d("APP_LIFE_CYCLE", "String[] permissions = " + s);
        for (int i: grantResults)
            Log.d("APP_LIFE_CYCLE", "int[] grantResults = " + i);

        // Return if nothing or more than one permission has been granted.
        if (grantResults.length == 0) {
            Log.d(this.getClass().getName(), getString(R.string.no_perm_granted));
            return;
        }

        if (requestCode != GET_LOCATION_FINE) {
            Log.d(this.getClass().getName(), getString(R.string.no_known_perm_granted));
            return;
        }

        for (int currentGrantResult : grantResults) {
            if (currentGrantResult == PackageManager.PERMISSION_GRANTED
                    || UsageManager.checkPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Enable the user's own location
                mMap.setMyLocationEnabled(true);

                // Request periodically location updates.
                LocationUtils.shared().getPeriodicLocationUpdates(getActivity());

                Toast.makeText(getActivity(), R.string.gps_granted, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getActivity(), R.string.gps_not_granted, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d("APP_LIFE_CYCLE", "MapsFragment: public void onLocationChanged(Location location)");
        // NOTE: There hasn't to be done anything when the location has been changed. (no camera move etc.)
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.d("APP_LIFE_CYCLE", "MapsFragment: public void onStatusChanged(String provider, int status, Bundle extras)");
        Log.d("APP_LIFE_CYCLE", "String provider = " + provider);
        Log.d("APP_LIFE_CYCLE", "int status = " + status);

        Context context = getContext();

        if (status == LocationProvider.OUT_OF_SERVICE)
            LocationUtils.shared().getLocationManager(context).removeUpdates(this);
        if (status == LocationProvider.TEMPORARILY_UNAVAILABLE)
            LocationUtils.shared().getLocationManager(context).removeUpdates(this);
        if (status == LocationProvider.AVAILABLE)
            LocationUtils.shared().getPeriodicLocationUpdates(context, provider);
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.d("APP_LIFE_CYCLE", "MapsFragment: public void onProviderEnabled(String provider)");
        Log.d("APP_LIFE_CYCLE", "String provider = " + provider);

        Toast.makeText(getActivity(), R.string.gps_activated, Toast.LENGTH_SHORT).show();
        if (UsageManager.checkPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)) {
            Context context = getContext();
            Location lastLocation = LocationUtils.shared().getLastKnownLocation(context, provider);
            if (lastLocation != null)
                moveCamera(lastLocation);
            LocationUtils.shared().getPeriodicLocationUpdates(context, provider);
        }
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.d("APP_LIFE_CYCLE", "MapsFragment: public void onProviderDisabled(String provider)");
        Log.d("APP_LIFE_CYCLE", "String provider = " + provider);

        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
        Toast.makeText(getContext(), getString(R.string.gps_not_activated),
                Toast.LENGTH_SHORT).show();
        LocationManager locationManager = LocationUtils.shared().getLocationManager(getContext());
        locationManager.removeUpdates(this);
    }

    /**
     * Displaying a marker in a specified location in the map.
     *
     * @param market Market which's location-marker has to be displayed in the map.
     */
    private void addMarker(Market market) {
        Marker newMarker;

        if (mMap == null)
            return;

        // Check if markers has to be initialized
        if (markers == null)
            markers = new ArrayList<>();

        // Check if location is given
        if (market == null)
            return;

        // Convert location to the needed type
        LatLng location = new LatLng(market.getLatitude(), market.getLongitude());

        // Create/update position marker in the map
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(location).snippet(getMapSnippet(market)).title(market.getPlace());
        newMarker = mMap.addMarker(markerOptions);
        markers.add(newMarker);
    }


    /**
     * Moves camera to the given location.
     *
     * @param loca Target location of the camera
     */
    private void moveCamera(Location loca) {
        CameraUpdate camUpdate;

        LatLng location = new LatLng(loca.getLatitude(), loca.getLongitude());
        /*
      Default zoom factor
     */
        final float DEFAULT_ZOOM_LEVEL = 12;
        camUpdate = CameraUpdateFactory.newLatLngZoom(location, DEFAULT_ZOOM_LEVEL);

        mMap.moveCamera(camUpdate);
    }

    /**
     * Retrieves information for snippet from the provided market
     *
     * @param currentMarket current market to get information for snippet
     */
    private String getMapSnippet(Market currentMarket) {
        if (!isAdded())
            return "";
        return getString(R.string.snippet_district) + " " + currentMarket.getDistrict() + "\n"
                + getString(R.string.snippet_days) + " " + currentMarket.getDays() + "\n"
                + getString(R.string.snippet_hours) + " " + currentMarket.getHours() + "\n";
    }

    /**
     * Creates market object by a GoogleMap Marker object.
     * @param marker Given marker which holds some informations
     * @return Displayed marker in the annotation
     */
    private Market getMarketFromSnippet(Marker marker) {
        List<Market> markets = DataManager.shared().getAllMarkets();
        String marketTitle = marker.getTitle();
        for (Market m : markets) {
            if (m.getPlace().equals(marketTitle))
                return m;
        }
        return null;
    }

    /**
     * Removing all markers on the map which are currently displayed.
     */
    private void resetMarkers() {
        if (markers != null) {
            if (!markers.isEmpty()) {
                for (Marker m : markers) {
                    m.remove();
                }
            }
        }
    }

    /**
     * Updates all markers in the map by removing all of them and adding the currently stored
     * markets from DataManager to the fragment as markers.
     */
    public void showAllMarketMarkers() {
        List<Market> markets = DataManager.shared().getAllMarkets();

        resetMarkers();

        for (Market m : markets)
            addMarker(m);
    }

    /**
     * Displaying all markets in the map which are opening now.
     */
    private void showOpenedMarketMarkers() {
        List<Market> markets = DataManager.shared().getOpenedMarkets();

        resetMarkers();

        for (Market m : markets)
            addMarker(m);
    }

    /**
     * Displaying all markets in the map wihch are opening at the current day.
     */
    private void showTodayMarketMarkers() {
        List<Market> markets = DataManager.shared().getTodayOpenedMarkets();

        resetMarkers();

        for (Market m : markets)
            addMarker(m);
    }
}
