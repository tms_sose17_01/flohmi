/*
 * This file is part of FlohmiApp.
 *
 * All rights reserved.
 */
package com.htw_berlin.tms_ss17.flohmi.utils;

import android.os.AsyncTask;
import android.util.Log;

import com.htw_berlin.tms_ss17.flohmi.model.Market;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Handling network requests and responses between the client and the REST-API.
 *
 * @author Semih Kasap
 * @author Max Scheibke
 * @author Yannick Vahldieck
 */

public class NetworkManager extends AsyncTask<String, Void, String> {

    /** Instance which has to be called when network processing has been finished. */
    private final NetworkManagerCallback callback;

    public NetworkManager(NetworkManagerCallback callback) {
        this.callback = callback;
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            String url = params[0];

            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            // optional default is GET
            con.setRequestMethod("GET");

            //add request header
            con.setRequestProperty("User-Agent", "Mozilla/5.0");

            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + url);
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            // string in json Format
            JSONObject jobj = new JSONObject(response.toString());

            JSONArray arr = jobj.getJSONArray("index");
            List<Market> marketsFromResponse = new ArrayList<>();
            for (int i = 0; i < arr.length(); i++) {
                Market tmp = new Market(arr.getJSONObject(i));
                marketsFromResponse.add(tmp);
            }
            DataManager.shared().updateMarket(marketsFromResponse);
            Log.i(getClass().getName(), "API: Number of markets = " + DataManager.shared().countMarkets());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Executed";
    }

    @Override
    protected void onPostExecute(String result) {
        // Telling the caller that the network request has been complete.
        callback.onNetworkRequestResult();
    }
}
