/*
 * This file is part of FlohmiApp.
 *
 * All rights reserved.
 */
package com.htw_berlin.tms_ss17.flohmi.persistence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.htw_berlin.tms_ss17.flohmi.model.Market;
import com.htw_berlin.tms_ss17.flohmi.utils.DataManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Helper class which is using the SQLOpenHelper for the markets database.
 *
 * @author Semih Kasap
 * @author Max Scheibke
 * @author Yannick Vahldieck
 *
 * @see MarketSQLiteHelper
 */
public class MarketsDataSource {

    /**
     * SQLite database instance.
     */
    private SQLiteDatabase database;

    /**
     * Helper for the markets database which is a SQLite database.
     */
    private final MarketSQLiteHelper databaseHelper;

    /**
     * All keys for all columns in the market table.
     */
    private final String[] allColumns = {
            MarketSQLiteHelper.MARKET_ID,
            MarketSQLiteHelper.MARKET_LATITUDE,
            MarketSQLiteHelper.MARKET_LONGITUDE,
            MarketSQLiteHelper.MARKET_IS_FAVORITE,
            MarketSQLiteHelper.MARKET_DISTRICT,
            MarketSQLiteHelper.MARKET_PLACE,
            MarketSQLiteHelper.MARKET_DAYS,
            MarketSQLiteHelper.MARKET_HOURS,
            MarketSQLiteHelper.MARKET_OPERATOR,
            MarketSQLiteHelper.MARKET_EMAIL,
            MarketSQLiteHelper.MARKET_WWW,
            MarketSQLiteHelper.MARKET_NOTICE,
            MarketSQLiteHelper.MARKET_CURRENT_DISTANCE
    };

    /**
     * Instantiate markets datasource.
     * @param context Context
     */
    public MarketsDataSource(Context context) {
        databaseHelper = new MarketSQLiteHelper(context);
    }

    /**
     * Opening the database with read and write permissions.
     */
    public void open() {
        try {
            Log.d(this.getClass().getName(), "Database opened. (read and write permissions)");
            database = databaseHelper.getWritableDatabase();
        } catch (SQLiteException e) {
            Log.e(this.getClass().getName(), "Database couldn't be opened for write access.");
            Log.e(this.getClass().getName(), e.getMessage());
        } catch (Exception e) {
            Log.e(this.getClass().getName(), "There was an error at opening the database with read write access:");
            Log.e(this.getClass().getName(), e.getMessage());
        }
    }

    /**
     * Close the database connection.
     */
    public void close() {
        databaseHelper.close();
    }

    /**
     * Insert a new row in the markets table.
     * @param market Market, which has to be inserted.
     * @return Market object which has been inserted.
     */
    private Market createMarket(Market market) {
        ContentValues values = new ContentValues();
        values.put(MarketSQLiteHelper.MARKET_ID, market.getId());
        values.put(MarketSQLiteHelper.MARKET_LATITUDE, market.getLatitude());
        values.put(MarketSQLiteHelper.MARKET_LONGITUDE, market.getLongitude());
        values.put(MarketSQLiteHelper.MARKET_IS_FAVORITE, market.getFavorite());
        values.put(MarketSQLiteHelper.MARKET_DISTRICT, market.getDistrict());
        values.put(MarketSQLiteHelper.MARKET_PLACE, market.getPlace());
        values.put(MarketSQLiteHelper.MARKET_DAYS, market.getDays());
        values.put(MarketSQLiteHelper.MARKET_HOURS, market.getHours());
        values.put(MarketSQLiteHelper.MARKET_OPERATOR, market.getOperator());
        values.put(MarketSQLiteHelper.MARKET_EMAIL, market.getEmail());
        values.put(MarketSQLiteHelper.MARKET_WWW, market.getWww());
        values.put(MarketSQLiteHelper.MARKET_NOTICE, market.getNotices());
        values.put(MarketSQLiteHelper.MARKET_CURRENT_DISTANCE, market.getCurrentDistance());

        Log.d(this.getClass().getName(), "Inserting new market to database: " + market.getLatitude() + " / " + market.getLongitude() + " (" + market + ")");

        long insertId = database.insert(MarketSQLiteHelper.TABLE_NAME, null, values);
        Cursor c = database.query(MarketSQLiteHelper.TABLE_NAME, allColumns, MarketSQLiteHelper.MARKET_ID + " = " + insertId, null, null, null, null);
        c.moveToFirst();
        Market newMarket = cursorToMarket(c);
        c.close();
        return newMarket;
    }

    /**
     * Delete a row by market object.
     * @param market Market
     */
    private void deleteMarket(Market market) {
        long id = market.getId();
        Log.d(this.getClass().getName(), "Delete market with id " + id);
        database.delete(MarketSQLiteHelper.TABLE_NAME, MarketSQLiteHelper.MARKET_ID + " = " + id, null);
    }

    /**
     * Delete a row by a market id.
     * @param marketId ID of the market.
     */
    public void deleteMarket(long marketId) {
        Log.d(this.getClass().getName(), "Delete market with id " + marketId);
        database.delete(MarketSQLiteHelper.TABLE_NAME, MarketSQLiteHelper.MARKET_ID + " = " + marketId, null);
    }

    public void updateMarket(Market newMarketAttributes) {
        Log.d(this.getClass().getName(), "Updating market with id " + newMarketAttributes.getId() + ": " + newMarketAttributes.toString());
        Market oldMarketAttributes;
        Cursor cursor;

        // Try to determine old market attributes
        String selection = MarketSQLiteHelper.MARKET_ID + " = " + "'" + newMarketAttributes.getId() + "'";
        //String[] selectionArgs = {  newMarketAttributes.getId() + "" };
        cursor = database.query(MarketSQLiteHelper.TABLE_NAME, allColumns, selection, null, null, null, null);
        if (cursor == null)
            Log.e(this.getClass().getName(), "Couldn't determine the old market which has to be updated.");

        if (cursor != null) {
            if (cursor.getCount() != 1) {
                Log.e(this.getClass().getName(), "Only one query result was expected.");
                createMarket(newMarketAttributes);
                return;
            }


            cursor.moveToNext();
            oldMarketAttributes = cursorToMarket(cursor);
            if (oldMarketAttributes == null) {
                Log.e(this.getClass().getName(), "Couldn't find the market which has to be updated.");
                return;
            }

            Log.d(this.getClass().getName(), "The old attributes are the following: " + oldMarketAttributes);
        }

        ContentValues values = new ContentValues(allColumns.length);
        values.put(MarketSQLiteHelper.MARKET_ID, newMarketAttributes.getId());
        values.put(MarketSQLiteHelper.MARKET_LATITUDE, newMarketAttributes.getLatitude());
        values.put(MarketSQLiteHelper.MARKET_LONGITUDE, newMarketAttributes.getLongitude());
        values.put(MarketSQLiteHelper.MARKET_IS_FAVORITE, newMarketAttributes.getFavorite());
        values.put(MarketSQLiteHelper.MARKET_DISTRICT, newMarketAttributes.getDistrict());
        values.put(MarketSQLiteHelper.MARKET_PLACE, newMarketAttributes.getPlace());
        values.put(MarketSQLiteHelper.MARKET_DAYS, newMarketAttributes.getDays());
        values.put(MarketSQLiteHelper.MARKET_HOURS, newMarketAttributes.getHours());
        values.put(MarketSQLiteHelper.MARKET_OPERATOR, newMarketAttributes.getOperator());
        values.put(MarketSQLiteHelper.MARKET_EMAIL, newMarketAttributes.getEmail());
        values.put(MarketSQLiteHelper.MARKET_WWW, newMarketAttributes.getWww());
        values.put(MarketSQLiteHelper.MARKET_NOTICE, newMarketAttributes.getNotices());
        values.put(MarketSQLiteHelper.MARKET_CURRENT_DISTANCE, newMarketAttributes.getCurrentDistance());

        int returnCode = database.update(MarketSQLiteHelper.TABLE_NAME, values, selection, null);
        if (returnCode != 1)
            Log.e(this.getClass().getName(), "There was a problem when updating one specific row in the database.");

        cursor = database.query(MarketSQLiteHelper.TABLE_NAME, allColumns, selection, null, null, null, null);
        cursor.moveToNext();
        Market testMarket = cursorToMarket(cursor);
        if (testMarket.getId() == newMarketAttributes.getId()
                && testMarket.getLatitude() == newMarketAttributes.getLatitude()
                && testMarket.getLongitude() == newMarketAttributes.getLongitude()
                && testMarket.getFavorite() == newMarketAttributes.getFavorite())
            Log.d(this.getClass().getName(), "Row was updated successfully!");
    }

    /**
     * Get all markets from database.
     * @return Markets from database.
     */
    public List<Market> getAllMarkets() {
        List<Market> markets = new ArrayList<>();

        Log.d(this.getClass().getName(), "Get all markets from database.");

        Cursor cursor = database.query(MarketSQLiteHelper.TABLE_NAME, allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Market comment = cursorToMarket(cursor);
            markets.add(comment);
            cursor.moveToNext();
        }

        Log.d(this.getClass().getName(), "Found " + markets.size() + " markets in the database table.");

        cursor.close();
        return markets;
    }

    /**
     * Get market by cursor.
     * @param cursor Cursor of a market row.
     * @return Market of the cursor.
     */
    private Market cursorToMarket(Cursor cursor) {
        Market market = new Market(-1, 0.0, 0.0);
        Log.d(this.getClass().getName(), "Pointing cursor to a market row...");
        market.setId(cursor.getInt(MarketSQLiteHelper.MARKET_COLUMN_ID));
        market.setLatitude(cursor.getDouble(MarketSQLiteHelper.MARKET_COLUMN_LATITUDE));
        market.setLongitude(cursor.getDouble(MarketSQLiteHelper.MARKET_COLUMN_LONGITUDE));

        int isFavorite = cursor.getInt(MarketSQLiteHelper.MARKET_COLUMN_IS_FAVORITE);
        if (isFavorite == 0)
            market.setFavorite(false);
        else if (isFavorite == 1)
            market.setFavorite(true);
        else
            Log.w(this.getClass().getName(), "The current row " + market.getId() + " returned a wrong value for is_favorite. (value: " + isFavorite + ")");

        market.setDistrict(cursor.getString(MarketSQLiteHelper.MARKET_COLUMN_DISTRICT));
        market.setPlace(cursor.getString(MarketSQLiteHelper.MARKET_COLUMN_PLACE));
        market.setDays(cursor.getString(MarketSQLiteHelper.MARKET_COLUMN_DAYS));
        market.setHours(cursor.getString(MarketSQLiteHelper.MARKET_COLUMN_HOURS));
        market.setOperator(cursor.getString(MarketSQLiteHelper.MARKET_COLUMN_OPERATOR));
        market.setEmail(cursor.getString(MarketSQLiteHelper.MARKET_COLUMN_EMAIL));
        market.setWww(cursor.getString(MarketSQLiteHelper.MARKET_COLUMN_WWW));
        market.setNotices(cursor.getString(MarketSQLiteHelper.MARKET_COLUMN_NOTICE));
        market.setCurrentDistance(cursor.getFloat(MarketSQLiteHelper.MARKET_COLUMN_CURRENT_DISTANCE));

        Log.d(this.getClass().getName(), "... " + market.toString());

        return market;
    }

    /**
     * Delete all markets from database and the runtime markets too.
     */
    public void resetMarkets() {
        List<Market> currentMarkets = getAllMarkets();

        // Check if there are already markets in the database
        if (currentMarkets == null)
            return;

        // Delete all markets
        for (Market m: currentMarkets)
            deleteMarket(m);

        // Check if all markets has been deleted successfully
        currentMarkets = getAllMarkets();
        if (!currentMarkets.isEmpty())
            Log.d(this.getClass().getName(), "Markets couldn't be deleted from Database.");
        else
            Log.d(this.getClass().getName(), "Markets have been successfully deleted from Database.");

        // Reset also runtime markets
        DataManager.shared().resetMarkets();
    }
}
