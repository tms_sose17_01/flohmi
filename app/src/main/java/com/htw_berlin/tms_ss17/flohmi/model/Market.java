/*
 * This file is part of FlohmiApp.
 *
 * All rights reserved.
 */
package com.htw_berlin.tms_ss17.flohmi.model;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Model class which represents market attributes. A market object can also be a favorite one or store
 * the distance to the user's current location.
 *
 * @author Semih Kasap
 * @author Max Scheibke
 * @author Yannick Vahldieck
 */
public class Market {

    private int id = 0;
    private String district = "", place = "", days = "", hours = "", operator = "", email = "", www = "", notices = "";
    private double latitude = 0.0f, longitude = 0.0f;
    private float currentDistance = 0.0f;
    private boolean isFavorite = false;
    private boolean[] weekdays = new boolean[7];
    private final List<Date[]> timesList = new ArrayList<>();

    public Market(JSONObject json) {
        this.id = json.optInt("id", 0);
        this.district = json.optString("bezirk", "");
        this.place = json.optString("location", "");
        this.days = json.optString("tage", "");
        this.hours = json.optString("zeiten", "");
        this.operator = json.optString("betreiber", "");
        this.email = json.optString("email", "");
        this.www = json.optString("www", "");
        this.notices = json.optString("bemerkungen", "");
        this.latitude = convertDecimalFromJSON(json, "latitude");
        this.longitude = convertDecimalFromJSON(json, "longitude");
    }

    public Market(int id, double latitude, double longitude) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    private double convertDecimalFromJSON(JSONObject json, String toConvert) {
        String temp = json.optString(toConvert, "0.0");
        temp = temp.replace(",", ".");
        return Double.valueOf(temp);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public String getHours() {
        return hours;
    }

    public void setHours(String hours) {
        analyzeTime(hours);
        this.hours = hours;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWww() {
        return www;
    }

    public void setWww(String www) {
        this.www = www;
    }

    public String getNotices() {
        return notices;
    }

    public void setNotices(String notices) {
        this.notices = notices;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setFavorite(boolean isFavorite) {
        this.isFavorite = isFavorite;
    }

    public boolean getFavorite() {
        return isFavorite;
    }

    public void setCurrentDistance(float currentDistance) {
        this.currentDistance = currentDistance;
    }

    public float getCurrentDistance() {
        return currentDistance;
    }

    @Override
    public String toString() {
        return "Markt {"
                + "id = " + id
                + ", Bezirk = '" + district + '\''
                + ", Ort = '" + place + '\''
                + ", Tage = '" + days + '\''
                + ", Zeiten = '" + hours + '\''
                + ", Betreiber = '" + operator
                + '\'' + ", Email = '" + email + '\''
                + ", www = '" + www + '\''
                + ", Bemerkungen = '" + notices + '\''
                + ", is Favorit? = '" + isFavorite + '\'' + '}';
    }

    public boolean[] getWeekDays() {
        this.weekdays = analyzeWeekdays(days);
        return weekdays;
    }

    public void setWeekdays(String days) {
        this.weekdays = analyzeWeekdays(days);
    }

    public void setWeekdays(boolean[] days) {
        if (days.length == 7)
            this.weekdays = days;
    }

    /**
     * Deserializing a market's opening days.
     * @param weekdays Open days of a market
     * @return true, for all open days
     */
    private boolean[] analyzeWeekdays(String weekdays) {
        boolean[] newWeekdays;

        if (weekdays == null)
            return null;

        newWeekdays = new boolean[7];
        newWeekdays[0] = Pattern.matches(".*(So).*", weekdays); // Sunday
        newWeekdays[1] = Pattern.matches(".*(Mo).*", weekdays); // Monday
        newWeekdays[2] = Pattern.matches(".*(Di).*", weekdays); // Tuesday
        newWeekdays[3] = Pattern.matches(".*(Mi).*", weekdays); // Wednesday
        newWeekdays[4] = Pattern.matches(".*(Do).*", weekdays); // Thursday
        newWeekdays[5] = Pattern.matches(".*(Fr).*", weekdays); // Friday
        newWeekdays[6] = Pattern.matches(".*(Sa).*", weekdays); // Saturday
        return newWeekdays;
    }

    /**
     * Check if a market is open to a given specific time.
     * @param currentDate Given time
     * @return true, if market is open
     */
    public boolean isHours(Date currentDate) {
        for (Date[] t: timesList) {
            if (t[0].compareTo(t[1]) <= 0
                && currentDate.compareTo(t[0]) >= 0
                && currentDate.compareTo(t[1]) <= 0)
                return true;
        }
        return false;
    }

    /**
     * Deserializing a string with a time interval in Date objects.
     * @param time Given time interval as a string
     */
    public void analyzeTime(String time) {
        Pattern p = Pattern.compile("([01]?[0-9]|2[0-3]):[0-5][0-9]");
        Matcher m = p.matcher(time);

        SimpleDateFormat format = new SimpleDateFormat("hh:mm", Locale.GERMANY);

        Date date;
        Date[] fromUntil = new Date[2];
        int i = 0;

        this.timesList.clear();
        while (m.find()) {
            try {
                date = format.parse(m.group());
                fromUntil[i % 2] = date;
                if (i % 2 == 1) {
                    setTimes(fromUntil);
                }
            } catch (Exception ignored) {
            }
            i++;
        }
    }

    /**
     * Analyzing current objects time intervals.
     */
    public void analyzeTime() {
        analyzeTime(this.hours);
    }

    /**
     * Setting opening hours as date objects.
     * @param times Start and beginning time of a market
     */
    public void setTimes(Date[] times) {
        if (times.length != 2)
            return;

        if (times[0].compareTo(times[1]) >= 0)
            return;

        timesList.add(times);
    }

}
