/*
 * This file is part of FlohmiApp.
 *
 * All rights reserved.
 */
package com.htw_berlin.tms_ss17.flohmi.fragments;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.htw_berlin.tms_ss17.flohmi.model.Market;
import com.htw_berlin.tms_ss17.flohmi.R;
import com.htw_berlin.tms_ss17.flohmi.fragments.FavoritesFragment.OnListFragmentInteractionListener;

import java.util.List;

/**
 * Favorites Adapter
 *
 * @author Semih Kasap
 * @author Max Scheibke
 * @author Yannick Vahldieck
 */
class MyFavoritesRecyclerViewAdapter extends RecyclerView.Adapter<MyFavoritesRecyclerViewAdapter.ViewHolder> {

    private final List<Market> mValues;
    private final OnListFragmentInteractionListener mListener;

    MyFavoritesRecyclerViewAdapter(List<Market> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_favorites, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        StringBuilder sb = new StringBuilder();
        sb.append(String.valueOf(mValues.get(position).getCurrentDistance()));
        sb.delete(sb.indexOf("."), sb.length());
        sb.append("m");

        holder.mDistanceView.setText(sb.toString());
        holder.mContentView.setText(holder.mItem.getPlace());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        final TextView mDistanceView;
        final TextView mContentView;
        Market mItem;

        ViewHolder(View view) {
            super(view);
            mView = view;
            mDistanceView = (TextView) view.findViewById(R.id.favorites_distance);
            mContentView = (TextView) view.findViewById(R.id.favorites_content);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
