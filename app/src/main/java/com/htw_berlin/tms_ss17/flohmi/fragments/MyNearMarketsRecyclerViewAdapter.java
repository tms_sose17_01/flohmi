/*
 * This file is part of FlohmiApp.
 *
 * All rights reserved.
 */
package com.htw_berlin.tms_ss17.flohmi.fragments;

import android.support.v7.util.SortedList;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.htw_berlin.tms_ss17.flohmi.model.Market;
import com.htw_berlin.tms_ss17.flohmi.R;
import com.htw_berlin.tms_ss17.flohmi.fragments.NearMarketsFragment.OnListFragmentInteractionListener;

import java.util.List;

/**
 * NearMarkets Adapter
 *
 * @author Semih Kasap
 * @author Max Scheibke
 * @author Yannick Vahldieck
 */
class MyNearMarketsRecyclerViewAdapter extends RecyclerView.Adapter<MyNearMarketsRecyclerViewAdapter.ViewHolder> {

    private final OnListFragmentInteractionListener mListener;
    private final SortedList<Market> sortedMarkets;

    MyNearMarketsRecyclerViewAdapter(List<Market> items, OnListFragmentInteractionListener listener) {
        mListener = listener;

        sortedMarkets = new SortedList<>(Market.class, new SortedList.Callback<Market>() {
            @Override
            public void onInserted(int position, int count) {
                notifyItemRangeChanged(position, count);
            }

            @Override
            public void onRemoved(int position, int count) {
                notifyItemRangeRemoved(position, count);
            }

            @Override
            public void onMoved(int fromPosition, int toPosition) {
                notifyItemMoved(fromPosition, toPosition);
            }

            @Override
            public int compare(Market o1, Market o2) {
                if (o1.getCurrentDistance() < o2.getCurrentDistance()) {
                    return -1;
                } else if (o1.getCurrentDistance() > o2.getCurrentDistance()) {
                    return 1;
                } else return 0;
            }

            @Override
            public void onChanged(int position, int count) {
                notifyItemRangeChanged(position, count);
            }

            @Override
            public boolean areContentsTheSame(Market oldItem, Market newItem) {
                return oldItem.getPlace().equals(newItem.getPlace());
            }

            @Override
            public boolean areItemsTheSame(Market item1, Market item2) {
                return item1.getId() == item2.getId();
            }
        });
        sortedMarkets.addAll(items);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_nearmarkets, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        holder.mItem = sortedMarkets.get(position);
        holder.mContentView.setText(holder.mItem.getPlace());
        StringBuilder sb = new StringBuilder();
        sb.append(String.valueOf(holder.mItem.getCurrentDistance()));
        sb.delete(sb.indexOf("."), sb.length());
        sb.append("m");

        holder.mDistanceView.setText(sb.toString());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return sortedMarkets.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        final TextView mContentView;
        final TextView mDistanceView;
        Market mItem;

        ViewHolder(View view) {
            super(view);
            mView = view;
            mContentView = (TextView) view.findViewById(R.id.nearmarkets_content);
            mDistanceView = (TextView) view.findViewById(R.id.nearmarkets_distance);

        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
