/*
 * This file is part of FlohmiApp.
 *
 * All rights reserved.
 */
package com.htw_berlin.tms_ss17.flohmi.utils;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import com.htw_berlin.tms_ss17.flohmi.R;

/**
 * LocationUtils holds a singleton instance of android LocationManager service.
 *
 * @author Semih Kasap
 * @author Max Scheibke
 * @author Yannick Vahldieck
 */
public final class LocationUtils {

    /** Minimum time until the location will be updated. */
    private final long MIN_TIME_UNTIL_LOCA_UPDATE = 4;

    /** Minimum distance which is needed to get an location update. */
    private final float MIN_DISTANCE_UNTIL_LOCA_UPDATE = 35;

    /**
     * Singleton instance of this class
     */
    private static LocationUtils singleton = null;

    /**
     * LocationManager for retrieving device's current position.
     */
    private LocationManager locationManager = null;

    private LocationUtils() {
        // NOTE: empty/private constructor
    }

    /**
     * Returning the singleton instance.
     *
     * @return Singleton instance
     */
    public static LocationUtils shared() {
        Log.d(LocationUtils.class.getName(), "Get singleton instance of type LocationUtils.");
        if (singleton == null)
            singleton = new LocationUtils();
        return singleton;
    }

    /**
     * Returns a LocationManager service for requesting location updates.
     *
     * @param context LocationManager's context.
     * @return LocationManager instance
     */
    public LocationManager getLocationManager(Context context) {
        Log.d(LocationUtils.class.getName(), "Get location manager from context.");
        if (locationManager == null) {
            locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        }
        return locationManager;
    }

    /**
     * Determine the current position of the device's GPS sensor.
     * @param context Context which is necessary for LocationManager objects
     * @return Last known location of the user's device
     */
    public Location getLastKnownLocation(Context context) {
        Location initialLocation = null, initialLocationByGPS = null, initialLocationByNetwork = null;
        Boolean isGPSProvider, isNetworkProvider;

        if (!UsageManager.checkPermission(context, Manifest.permission.ACCESS_FINE_LOCATION))
            return null;

        isGPSProvider = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        isNetworkProvider = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        // Get last known location of the GPS sensor
        if (isGPSProvider)
            initialLocationByGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        if (isNetworkProvider)
            initialLocationByNetwork = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

        if (initialLocationByGPS != null && initialLocationByNetwork != null) {
            if (initialLocationByGPS.getAccuracy() > initialLocationByNetwork.getAccuracy())
                initialLocation = initialLocationByGPS;
            else
                initialLocation = initialLocationByNetwork;
        } else if (initialLocationByGPS != null) {
            initialLocation = initialLocationByGPS;
        } else if (initialLocationByNetwork != null) {
            initialLocation = initialLocationByNetwork;
        } else {
            Log.d(this.getClass().getName(), "No last location could have been determined.");
        }
        return initialLocation;
    }

    /**
     * Determine the last known location to a specific provider.
     * @param context Necessary for LocationManager objects
     * @param provider Given provider which should be used
     * @return Last known position by a specific provider
     */
    public Location getLastKnownLocation(Context context, String provider) {
        if (!UsageManager.checkPermission(context, Manifest.permission.ACCESS_FINE_LOCATION))
            return null;

        if (locationManager.isProviderEnabled(provider))
            return locationManager.getLastKnownLocation(provider);
        else
            return null;
    }

    /**
     * Get periodic location updates.
     * @param context Necessary for LocationManager
     */
    public void getPeriodicLocationUpdates(Context context) {
        Boolean isGPSProvider, isNetworkProvider;

        if (!UsageManager.checkPermission(context, Manifest.permission.ACCESS_FINE_LOCATION))
            return;

        isGPSProvider = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        isNetworkProvider = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        // Start requesting location updates
        // INFO: The location is getting request periodically all 4 s or 35 m.
        if (isGPSProvider) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_UNTIL_LOCA_UPDATE, MIN_DISTANCE_UNTIL_LOCA_UPDATE, (LocationListener) context);
        } else if (isNetworkProvider) {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_UNTIL_LOCA_UPDATE, MIN_DISTANCE_UNTIL_LOCA_UPDATE, (LocationListener) context);
        } else {
            Log.d(this.getClass().getName(), context.getString(R.string.no_prov_enabl));
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            context.startActivity(intent);
            Toast.makeText(context, R.string.gps_not_activated, Toast.LENGTH_SHORT).show();
        }
        Log.d(this.getClass().getName(), context.getString(R.string.req_periodic_loca));
    }

    /**
     * Get periodic location updates by a provider.
     * @param context Necessary for LocationManager
     * @param provider Specific provider
     */
    public void getPeriodicLocationUpdates(Context context, String provider) {
        if (!UsageManager.checkPermission(context, Manifest.permission.ACCESS_FINE_LOCATION))
            return;

        if (locationManager.isProviderEnabled(provider)) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_UNTIL_LOCA_UPDATE, MIN_DISTANCE_UNTIL_LOCA_UPDATE, (LocationListener) context);
        } else {
            Log.d(this.getClass().getName(), context.getString(R.string.no_prov_enabl));
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            context.startActivity(intent);
            Toast.makeText(context, R.string.gps_not_activated, Toast.LENGTH_SHORT).show();
        }
        Log.d(this.getClass().getName(), context.getString(R.string.req_periodic_loca));
    }
}