/*
 * This file is part of FlohmiApp.
 *
 * All rights reserved.
 */
package com.htw_berlin.tms_ss17.flohmi.utils;


import android.location.Location;

import com.google.android.gms.maps.model.LatLng;
import com.htw_berlin.tms_ss17.flohmi.model.Market;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The datamanager of the app is the location for all stored business data.
 *
 * @author Semih Kasap
 * @author Max Scheibke
 * @author Yannick Vahldieck
 */
public final class DataManager {

    /**
     * Singleton instance of the current class
     */
    private static DataManager singleton = null;

    /**
     * Currently stored markets at runtime.
     */
    private final Map<Integer, Market> markets;

    private DataManager() {
        // NOTE: Leave this private.
        markets = new HashMap<>();
    }

    /**
     * Get the singleton instance with all it's business data.
     *
     * @return Datamanager instance
     */
    public static DataManager shared() {
        if (singleton == null)
            singleton = new DataManager();
        return singleton;
    }

    /**
     * Adding new markets.
     *
     * @param newMarket New markets
     */
    public void addMarket(Market newMarket) {
        markets.put(newMarket.getId(), newMarket);
    }

    /**
     * Adding new markets.
     *
     * @param newMarkets New markets as a list.
     */
    public void addMarket(List<Market> newMarkets) {
        if (newMarkets.isEmpty())
            return;

        for (Market m : newMarkets)
            markets.put(m.getId(), m);
    }

    /**
     * Updating markets and also removing not used markets.
     *
     * @param newMarkets New markets which have to be hold runtime.
     */
    void updateMarket(List<Market> newMarkets) {
        Map<Integer, Market> oldMarkets;

        if (newMarkets.isEmpty())
            return;

        oldMarkets = markets;

        if (!oldMarkets.isEmpty()) {
            // Add new markets
            for (Market m : newMarkets) {
                Market oldMarket = oldMarkets.get(m.getId());
                if (oldMarket != null) {
                    m.setFavorite(oldMarket.getFavorite());
                    m.setCurrentDistance(oldMarket.getCurrentDistance());
                    oldMarkets.remove(m.getId());
                }
                markets.put(m.getId(), m);
            }

            // Remove not needed markets
            if (!oldMarkets.isEmpty()) {
                for (Integer id : oldMarkets.keySet())
                    markets.remove(id);
            }
        } else {
            for (Market m : newMarkets)
                markets.put(m.getId(), m);
        }
    }

    /**
     * Removing all currently stored markets.
     */
    public void resetMarkets() {
        markets.clear();
    }

    /**
     * Returns the amount of markets.
     *
     * @return Amount of markets
     */
    int countMarkets() {
        return markets.size();
    }

    /**
     * Return all markets which are currently stored at runtime.
     *
     * @return Currently stored markets.
     */
    public List<Market> getAllMarkets() {
        return new ArrayList<>(markets.values());
    }

    /**
     * Returning all currently stored markets at runtime which are marked as favorites.
     *
     * @return Favorite markets
     */
    public List<Market> getFavoriteMarkets() {
        List<Market> favMarkets = new ArrayList<>();

        for (Market m : markets.values()) {
            // Check if the current market was faved.
            if (m.getFavorite())
                favMarkets.add(m);
        }

        return favMarkets;
    }

    /**
     * Return all markets between one given location and a given distance.
     *
     * @param latitude  Latitude of the given position
     * @param longitude Longitude of the given position
     * @param distance  Maximum distance of the markets in metres.
     * @return Array with all nearby markets
     */
    public List<Market> getNearbyMarkets(double latitude, double longitude, int distance) {
        List<Market> nearMarkets = new ArrayList<>();
        for (Market market : markets.values()) {
            double dLat = Math.toRadians(market.getLatitude() - latitude);
            double dLng = Math.toRadians(market.getLongitude() - longitude);
            double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                    + Math.cos(Math.toRadians(market.getLatitude())) * Math.cos(Math.toRadians(latitude))
                    * Math.sin(dLng / 2) * Math.sin(dLng / 2);
            double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

            final double EARTHRADIUS = 6371000;

            float dist = (float) (EARTHRADIUS * c);
            if (dist <= distance) {
                market.setCurrentDistance(dist);
                nearMarkets.add(market);
            }
        }
        return nearMarkets;
    }

    /**
     * Return all markets between one given location and a given distance.
     *
     * @param position Center position of the area
     * @param distance Maximum distance of the markets in metres.
     * @return Array with all nearby markets
     */
    public List<Market> getNearbyMarkets(Location position, int distance) {
        if (position != null)
            return getNearbyMarkets(position.getLatitude(), position.getLongitude(), distance);
        else
            return new ArrayList<>();
    }

    /**
     * Return all markets between one given location and a given distance.
     *
     * @param position Center position of the area
     * @param distance Maximum distance of the markets in metres.
     * @return Array with all nearby markets
     */
    public List<Market> getNearbyMarkets(LatLng position, int distance) {
        if (position != null)
            return getNearbyMarkets(position.latitude, position.longitude, distance);
        else
            return null;
    }

    /**
     * Update distances relative to a specific position.
     * @param location Specific position
     */
    public void updateNearbyValues(Location location) {
        if (location == null)
            return;

        final int MAX_DISTANCE = 4000;
        getNearbyMarkets(location, MAX_DISTANCE);
    }

    /**
     * Get one market and its attributes.
     *
     * @param id ID of an market (given by API)
     * @return Market
     */
    public Market getMarket(int id) {
        return markets.get(id);
    }

    /**
     * Determine all open markets to the current time.
     * @return Currently open markets
     */
    public List<Market> getOpenedMarkets() {
        List<Market> openMarkets = new ArrayList<>();

        Date currentDate = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(currentDate);
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
        c.set(Calendar.YEAR, 1970);
        c.set(Calendar.MONTH, 0);
        c.set(Calendar.DAY_OF_MONTH, 1);
        currentDate = c.getTime();

        for (Market m : markets.values()) {
            m.analyzeTime();
            if (m.getWeekDays()[dayOfWeek - 1] && m.isHours(currentDate))
                openMarkets.add(m);
        }

        return openMarkets;
    }

    /**
     * Determine all known markets which are open today.
     * @return Open market's at the current day
     */
    public List<Market> getTodayOpenedMarkets() {
        List<Market> todaysMarkets = new ArrayList<>();

        Date currentDate = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(currentDate);
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);

        for (Market m : markets.values()) {
            if (m.getWeekDays()[dayOfWeek - 1])
                todaysMarkets.add(m);
        }

        return todaysMarkets;
    }
}
