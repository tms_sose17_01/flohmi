/*
 * This file is part of FlohmiApp.
 *
 * All rights reserved.
 */
package com.htw_berlin.tms_ss17.flohmi.persistence;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Persistence Helper
 *
 * @author Semih Kasap
 * @author Max Scheibke
 * @author Yannick Vahldieck
 *
 * @see MarketSQLiteHelper
 */
final class MarketSQLiteHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 3;
    static final String TABLE_NAME = "market";
    static final String MARKET_ID = "marketId";
    static final int MARKET_COLUMN_ID = 0;
    static final String MARKET_LATITUDE = "latitude";
    static final int MARKET_COLUMN_LATITUDE = 1;
    static final String MARKET_LONGITUDE = "longitude";
    static final int MARKET_COLUMN_LONGITUDE = 2;
    static final String MARKET_IS_FAVORITE = "isFavorite";
    static final int MARKET_COLUMN_IS_FAVORITE = 3;
    static final String MARKET_DISTRICT = "district";
    static final int MARKET_COLUMN_DISTRICT = 4;
    static final String MARKET_PLACE = "place";
    static final int MARKET_COLUMN_PLACE = 5;
    static final String MARKET_DAYS = "days";
    static final int MARKET_COLUMN_DAYS = 6;
    static final String MARKET_HOURS = "hours";
    static final int MARKET_COLUMN_HOURS = 7;
    static final String MARKET_OPERATOR = "operator";
    static final int MARKET_COLUMN_OPERATOR = 8;
    static final String MARKET_EMAIL = "email";
    static final int MARKET_COLUMN_EMAIL = 9;
    static final String MARKET_WWW = "www";
    static final int MARKET_COLUMN_WWW = 10;
    static final String MARKET_NOTICE = "notice";
    static final int MARKET_COLUMN_NOTICE = 11;
    static final String MARKET_CURRENT_DISTANCE = "currentDistance";
    static final int MARKET_COLUMN_CURRENT_DISTANCE = 12;
    private static final String TABLE_CREATE =
            "CREATE TABLE " + TABLE_NAME + " ("
                    + MARKET_ID + " INTEGER PRIMARY KEY ASC, " // signed integer, stored in 1, 2, 3, 4, 6 or 8 bytes
                    + MARKET_LATITUDE + " REAL NOT NULL, " // floating point value, stored as 8-byte IEEE floating point number
                    + MARKET_LONGITUDE + " REAL NOT NULL, "
                    + MARKET_IS_FAVORITE + " INTEGER, "
                    + MARKET_DISTRICT + " TEXT, "
                    + MARKET_PLACE + " TEXT, "
                    + MARKET_DAYS + " TEXT, "
                    + MARKET_HOURS + " TEXT, "
                    + MARKET_OPERATOR + " TEXT, "
                    + MARKET_EMAIL + " TEXT, "
                    + MARKET_WWW + " TEXT, "
                    + MARKET_NOTICE + " TEXT, "
                    + MARKET_CURRENT_DISTANCE + " REAL"
                    + ")";

    MarketSQLiteHelper(Context context) {
        super(context, TABLE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(this.getClass().getName(), "Creating database " + TABLE_NAME);
        db.execSQL(TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(this.getClass().getName(), "Upgrading database from version " + oldVersion + " to " + newVersion + " by dropping all of its contents");

        if (oldVersion < 2) {
            String sqlStatement = "ALTER TABLE " + TABLE_NAME
                    + " ADD COLUMN "
                    + MARKET_IS_FAVORITE + " INTEGER;";
            Log.d(this.getClass().getName(), sqlStatement);
            db.execSQL(sqlStatement);
        }
        if (oldVersion < 3) {
            String sqlStatement, sqlStatementPrefix, sqlStatementSuffix;

            sqlStatementPrefix = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN ";

            sqlStatementSuffix = MARKET_DISTRICT + " TEXT;";
            sqlStatement = sqlStatementPrefix + sqlStatementSuffix;
            Log.d(this.getClass().getName(), sqlStatement);
            db.execSQL(sqlStatement);

            sqlStatementSuffix = MARKET_PLACE + " TEXT;";
            sqlStatement = sqlStatementPrefix + sqlStatementSuffix;
            Log.d(this.getClass().getName(), sqlStatement);
            db.execSQL(sqlStatement);

            sqlStatementSuffix = MARKET_DAYS + " TEXT;";
            sqlStatement = sqlStatementPrefix + sqlStatementSuffix;
            Log.d(this.getClass().getName(), sqlStatement);
            db.execSQL(sqlStatement);

            sqlStatementSuffix = MARKET_HOURS + " TEXT;";
            sqlStatement = sqlStatementPrefix + sqlStatementSuffix;
            Log.d(this.getClass().getName(), sqlStatement);
            db.execSQL(sqlStatement);

            sqlStatementSuffix = MARKET_OPERATOR + " TEXT;";
            sqlStatement = sqlStatementPrefix + sqlStatementSuffix;
            Log.d(this.getClass().getName(), sqlStatement);
            db.execSQL(sqlStatement);

            sqlStatementSuffix = MARKET_EMAIL + " TEXT;";
            sqlStatement = sqlStatementPrefix + sqlStatementSuffix;
            Log.d(this.getClass().getName(), sqlStatement);
            db.execSQL(sqlStatement);

            sqlStatementSuffix = MARKET_WWW + " TEXT;";
            sqlStatement = sqlStatementPrefix + sqlStatementSuffix;
            Log.d(this.getClass().getName(), sqlStatement);
            db.execSQL(sqlStatement);

            sqlStatementSuffix = MARKET_NOTICE + " TEXT;";
            sqlStatement = sqlStatementPrefix + sqlStatementSuffix;
            Log.d(this.getClass().getName(), sqlStatement);
            db.execSQL(sqlStatement);

            sqlStatementSuffix = MARKET_CURRENT_DISTANCE + " REAL;";
            sqlStatement = sqlStatementPrefix + sqlStatementSuffix;
            Log.d(this.getClass().getName(), sqlStatement);
            db.execSQL(sqlStatement);
        }
    }
}
