/*
 * This file is part of FlohmiApp.
 *
 * All rights reserved.
 */
package com.htw_berlin.tms_ss17.flohmi.utils;

/**
 * This Callback is needed for results from network API.
 *
 * @author Semih Kasap
 * @author Max Scheibke
 * @author Yannick Vahldieck
 */
public interface NetworkManagerCallback {

    /**
     * This method will be called when the network API's response has been processed and it's data
     * are stored in the DataManager.
     */
    void onNetworkRequestResult();
}
