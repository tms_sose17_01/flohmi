/*
 * This file is part of FlohmiApp.
 *
 * All rights reserved.
 */
package com.htw_berlin.tms_ss17.flohmi.utils;

import android.app.Activity;
import android.content.Context;

/**
 * Interface for the permissions for test runs
 *
 * @author Semih Kasap
 * @author Max Scheibke
 * @author Yannick Vahldieck
 */
public interface PermissionsModule {
    boolean isLocationGranted(Context context);
    boolean shouldShowLocationPermissionRationale(Activity activity);
    void requestLocationPermission(Activity activity, int requestCode);
}
