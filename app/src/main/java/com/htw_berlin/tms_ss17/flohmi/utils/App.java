/*
 * This file is part of FlohmiApp.
 *
 * All rights reserved.
 */
package com.htw_berlin.tms_ss17.flohmi.utils;

import android.app.Application;


/**
 * Helper Class to get current App instance for Tests
 *
 * @author Semih Kasap
 * @author Max Scheibke
 * @author Yannick Vahldieck
 */
public class App extends Application {
    private static PermissionsModule permissionsModule;
    private static App instance;


    @Override
    public void onCreate() {
        super.onCreate();

        setInstance(this);
        setPermissionsModule(new DefaultPermissionsModule());
    }

    private static void setInstance(App app) {
        instance = app;
    }

    public static App getInstance() {
        return instance;
    }

    public static PermissionsModule getPermissionsModule() {
        return permissionsModule;
    }

    public static void setPermissionsModule(PermissionsModule permissionsModule) {
        App.permissionsModule = permissionsModule;
    }
}
