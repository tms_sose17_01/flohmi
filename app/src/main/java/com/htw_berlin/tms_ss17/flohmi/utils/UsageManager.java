/*
 * This file is part of FlohmiApp.
 *
 * All rights reserved.
 */
package com.htw_berlin.tms_ss17.flohmi.utils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;

/**
 * The UsageManager ist for checking if a permission is already granted or request a permission.
 *
 * @author Semih Kasap
 * @author Max Scheibke
 * @author Yannick Vahldieck
 */
public class UsageManager {

    /**
     * Check if permission has been already granted.
     * @param context Context of the permission
     * @param permission Technical name of the permission
     * @return true, if granted
     */
    public static boolean checkPermission(Context context, String permission) {
        if (Build.VERSION.SDK_INT < 23)
            return false;

        if (ActivityCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED) {
            Activity activity = (Activity) context;
            activity.shouldShowRequestPermissionRationale(permission);
            return true;
        } else
            return false;
    }

    /**
     * Requesting a specific permission if it has not been granted already.
     * @param context Context of the permission
     * @param permission Technical name of the permission
     * @param requestCode Request code for checking which permission has been granted later.
     */
    public static void requestPermission(Context context, String permission, int requestCode) {
        String[] permissions = new String[1];
        permissions[0] = permission;
        if (!checkPermission(context, permission))
            ActivityCompat.requestPermissions((Activity) context, permissions, requestCode);
    }
}
