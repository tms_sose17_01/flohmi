/*
 * This file is part of FlohmiApp.
 *
 * All rights reserved.
 */
package com.htw_berlin.tms_ss17.flohmi.fragments;


import android.Manifest;
import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.htw_berlin.tms_ss17.flohmi.model.Market;
import com.htw_berlin.tms_ss17.flohmi.utils.NetworkManagerCallback;
import com.htw_berlin.tms_ss17.flohmi.R;
import com.htw_berlin.tms_ss17.flohmi.utils.UsageManager;
import com.htw_berlin.tms_ss17.flohmi.utils.DataManager;
import com.htw_berlin.tms_ss17.flohmi.utils.LocationUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * NearMarkets Fragment
 *
 * @author Semih Kasap
 * @author Max Scheibke
 * @author Yannick Vahldieck
 */
public class NearMarketsFragment extends Fragment implements NetworkManagerCallback {

    private static final String ARG_COLUMN_COUNT = "column-count";
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;

    private List<Market> nearMarketList = new ArrayList<>();

    public NearMarketsFragment() {
    }

    private void getNearbyMarkets() {
        if (!UsageManager.checkPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION))
            return;
        Location currentLocation = LocationUtils.shared().getLastKnownLocation(getActivity());
        if (currentLocation == null) {
            Toast.makeText(getActivity(), R.string.gps_not_granted, Toast.LENGTH_SHORT).show();
        } else {
            final int MAXDISTANCE = 4000;
            nearMarketList = DataManager.shared().getNearbyMarkets(currentLocation, MAXDISTANCE);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
        getNearbyMarkets();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_nearmarkets_list, container, false);

        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.nearmarkets));

        NavigationView navigationView = (NavigationView) getActivity().findViewById(R.id.nav_view);
        Menu menu = navigationView.getMenu();
        MenuItem item = menu.findItem(R.id.nav_nearMarkets);
        item.setChecked(true);

        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            recyclerView.setAdapter(new MyNearMarketsRecyclerViewAdapter(nearMarketList, mListener));
        }
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onNetworkRequestResult() {
    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(Market item);
    }
}
