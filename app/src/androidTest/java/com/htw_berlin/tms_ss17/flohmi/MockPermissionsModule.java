/*
 * This file is part of FlohmiApp.
 *
 * All rights reserved.
 */
package com.htw_berlin.tms_ss17.flohmi;

import android.app.Activity;
import android.content.Context;

import com.htw_berlin.tms_ss17.flohmi.utils.PermissionsModule;

/**
 * PermissionsModule Implementation for granting permissions to the tests
 *
 * @author Semih Kasap
 * @author Max Scheibke
 * @author Yannick Vahldieck
 */

class MockPermissionsModule implements PermissionsModule {
    private boolean locationGranted = false;

    @Override
    public boolean isLocationGranted(Context context) {
        return locationGranted;
    }

    @Override
    public boolean shouldShowLocationPermissionRationale(Activity activity) {
        return false;
    }

    @Override
    public void requestLocationPermission(Activity activity, int requestCode) {
    }

    void setLocationGranted(boolean locationGranted) {
        this.locationGranted = locationGranted;
    }
}
