/*
 * This file is part of FlohmiApp.
 *
 * All rights reserved.
 */
package com.htw_berlin.tms_ss17.flohmi;


import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.filters.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import com.htw_berlin.tms_ss17.flohmi.utils.App;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;


/**
 * UI-Test for navigating through Navigation Drawer
 *
 * Permissions have to be granted on first start of the test
 *
 * @author Semih Kasap
 * @author Max Scheibke
 * @author Yannick Vahldieck
 */

@LargeTest
@RunWith(AndroidJUnit4.class)
public class NavigationDrawerTest {
    private final MockPermissionsModule permissionsModule = new MockPermissionsModule();

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Before
    public void setUp() {
        App.setPermissionsModule(permissionsModule);
    }

    @Test
    public void navigationDrawerTest() {
        permissionsModule.setLocationGranted(true);

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ViewInteraction appCompatImageButton = onView(
                allOf(withContentDescription("Öffne Navigation"),
                        withParent(withId(R.id.toolbar)),
                        isDisplayed()));
        appCompatImageButton.perform(click());

        ViewInteraction recyclerView = onView(
                allOf(withId(R.id.design_navigation_view),
                        childAtPosition(
                                allOf(withId(R.id.nav_view),
                                        childAtPosition(
                                                withId(R.id.drawer_layout),
                                                1)),
                                0),
                        isDisplayed()));
        recyclerView.check(matches(isDisplayed()));

        ViewInteraction appCompatCheckedTextView = onView(
                allOf(withId(R.id.design_menu_item_text), withText("Märkte in der Nähe"), isDisplayed()));
        appCompatCheckedTextView.perform(click());

        ViewInteraction recyclerView2 = onView(
                allOf(withId(R.id.list),
                        childAtPosition(
                                allOf(withId(R.id.content_frame),
                                        childAtPosition(
                                                IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class),
                                                1)),
                                0),
                        isDisplayed()));
        recyclerView2.check(matches(isDisplayed()));

        ViewInteraction textView = onView(
                allOf(withText("Märkte in der Nähe"),
                        childAtPosition(
                                allOf(withId(R.id.toolbar),
                                        childAtPosition(
                                                IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class),
                                                0)),
                                1),
                        isDisplayed()));
        textView.check(matches(withText("Märkte in der Nähe")));

        ViewInteraction appCompatImageButton2 = onView(
                allOf(withContentDescription("Öffne Navigation"),
                        withParent(withId(R.id.toolbar)),
                        isDisplayed()));
        appCompatImageButton2.perform(click());

        ViewInteraction recyclerView3 = onView(
                allOf(withId(R.id.design_navigation_view),
                        childAtPosition(
                                allOf(withId(R.id.nav_view),
                                        childAtPosition(
                                                withId(R.id.drawer_layout),
                                                1)),
                                0),
                        isDisplayed()));
        recyclerView3.check(matches(isDisplayed()));

        ViewInteraction appCompatCheckedTextView2 = onView(
                allOf(withId(R.id.design_menu_item_text), withText("Favoriten"), isDisplayed()));
        appCompatCheckedTextView2.perform(click());

        ViewInteraction recyclerView4 = onView(
                allOf(withId(R.id.favorites_list),
                        childAtPosition(
                                allOf(withId(R.id.content_frame),
                                        childAtPosition(
                                                IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class),
                                                1)),
                                0),
                        isDisplayed()));
        recyclerView4.check(matches(isDisplayed()));

        ViewInteraction textView2 = onView(
                allOf(withText("Favoriten"),
                        childAtPosition(
                                allOf(withId(R.id.toolbar),
                                        childAtPosition(
                                                IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class),
                                                0)),
                                1),
                        isDisplayed()));
        textView2.check(matches(withText("Favoriten")));

        ViewInteraction appCompatImageButton3 = onView(
                allOf(withContentDescription("Öffne Navigation"),
                        withParent(withId(R.id.toolbar)),
                        isDisplayed()));
        appCompatImageButton3.perform(click());

        ViewInteraction recyclerView5 = onView(
                allOf(withId(R.id.design_navigation_view),
                        childAtPosition(
                                allOf(withId(R.id.nav_view),
                                        childAtPosition(
                                                withId(R.id.drawer_layout),
                                                1)),
                                0),
                        isDisplayed()));
        recyclerView5.check(matches(isDisplayed()));

        ViewInteraction appCompatCheckedTextView3 = onView(
                allOf(withId(R.id.design_menu_item_text), withText("Disclaimer"), isDisplayed()));
        appCompatCheckedTextView3.perform(click());

        ViewInteraction linearLayout = onView(
                allOf(childAtPosition(
                        childAtPosition(
                                withId(R.id.content_frame),
                                0),
                        0),
                        isDisplayed()));
        linearLayout.check(matches(isDisplayed()));

        ViewInteraction textView3 = onView(
                allOf(withText("Disclaimer"),
                        childAtPosition(
                                allOf(withId(R.id.toolbar),
                                        childAtPosition(
                                                IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class),
                                                0)),
                                1),
                        isDisplayed()));
        textView3.check(matches(withText("Disclaimer")));

    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
