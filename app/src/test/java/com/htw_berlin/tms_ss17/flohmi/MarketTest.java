/*
 * This file is part of FlohmiApp.
 *
 * All rights reserved.
 */
package com.htw_berlin.tms_ss17.flohmi;

import com.htw_berlin.tms_ss17.flohmi.model.Market;

import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static org.junit.Assert.*;

/**
 * Unit tests for the markets
 *
 * @author Semih Kasap
 * @author Max Scheibke
 * @author Yannick Vahldieck
 */
public class MarketTest {

    @Test
    public void determineWeekdaysSuccess01() throws Exception {
        Market cut = new Market(1, 0.0, 0.0);
        cut.setDays("Mo, Mi, So");
        assertTrue(cut.getWeekDays()[0]);
        assertTrue(cut.getWeekDays()[1]);
        assertFalse(cut.getWeekDays()[2]);
        assertTrue(cut.getWeekDays()[3]);
        assertFalse(cut.getWeekDays()[4]);
        assertFalse(cut.getWeekDays()[5]);
        assertFalse(cut.getWeekDays()[6]);
    }

    @Test
    public void determineWeekdaysSuccess02() throws Exception {
        Market cut = new Market(1, 0.0, 0.0);
        cut.setDays("Mo - Fr");
        assertFalse(cut.getWeekDays()[0]);
        assertTrue(cut.getWeekDays()[1]);
        assertFalse(cut.getWeekDays()[2]);
        assertFalse(cut.getWeekDays()[3]);
        assertFalse(cut.getWeekDays()[4]);
        assertTrue(cut.getWeekDays()[5]);
        assertFalse(cut.getWeekDays()[6]);
    }

    @Test
    public void determineWeekdaysSuccess03() throws Exception {
        Market cut = new Market(1, 0.0, 0.0);
        cut.setDays("Mo, Di, Do, So");
        assertTrue(cut.getWeekDays()[0]);
        assertTrue(cut.getWeekDays()[1]);
        assertTrue(cut.getWeekDays()[2]);
        assertFalse(cut.getWeekDays()[3]);
        assertTrue(cut.getWeekDays()[4]);
        assertFalse(cut.getWeekDays()[5]);
        assertFalse(cut.getWeekDays()[6]);
    }

    @Test
    public void setTimeSuccess01() throws Exception {
        SimpleDateFormat format = new SimpleDateFormat("hh:mm", Locale.GERMANY);
        Date currentDate = format.parse("13:30");

        Market cut = new Market(1, 0.0, 0.0);

        Date[] times = new Date[2];

        times[0] = format.parse("13:00");
        times[1] = format.parse("14:00");
        cut.setTimes(times);
        assertTrue(cut.isHours(currentDate));
    }

    @Test
    public void setTimeSuccess02() throws Exception {
        SimpleDateFormat format = new SimpleDateFormat("hh:mm", Locale.GERMANY);
        Date currentDate = format.parse("14:30");

        Market cut = new Market(1, 0.0, 0.0);

        Date[] times = new Date[2];

        times[0] = format.parse("14:00");
        times[1] = format.parse("15:00");
        cut.setTimes(times);
        assertTrue(cut.isHours(currentDate));
    }

    @Test
    public void setTimeFail01() throws Exception {
        SimpleDateFormat format = new SimpleDateFormat("hh:mm", Locale.GERMANY);
        Date currentDate = format.parse("14:30");

        Market cut = new Market(1, 0.0, 0.0);

        Date[] times = new Date[2];

        times[0] = format.parse("13:00");
        times[1] = format.parse("14:00");
        cut.setTimes(times);
        assertFalse(cut.isHours(currentDate));
    }

    @Test
    public void setTimeFail02() throws Exception {
        SimpleDateFormat format = new SimpleDateFormat("hh:mm", Locale.GERMANY);
        Date currentDate = format.parse("14:30");

        Market cut = new Market(1, 0.0, 0.0);

        Date[] times = new Date[2];

        times[0] = format.parse("8:00");
        times[1] = format.parse("14:00");
        cut.setTimes(times);
        assertFalse(cut.isHours(currentDate));
    }

    @Test
    public void setTimeFail03() throws Exception {
        SimpleDateFormat format = new SimpleDateFormat("hh:mm", Locale.GERMANY);
        Date currentDate = format.parse("7:30");

        Market cut = new Market(1, 0.0, 0.0);

        Date[] times = new Date[2];

        times[0] = format.parse("8:00");
        times[1] = format.parse("23:00");
        cut.setTimes(times);
        assertFalse(cut.isHours(currentDate));
    }

    @Test
    public void isHoursSuccess01() throws Exception {
        Market cut = new Market(1, 0.0, 0.0);

        // Create date of a specific time
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1970);
        cal.set(Calendar.MONTH, 0);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.HOUR_OF_DAY, 14);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        Date currentTime = cal.getTime();

        cut.setHours("13:00 - 15:00");
        assertTrue(cut.isHours(currentTime));
    }

    @Test
    public void isHoursSuccess03() throws Exception {
        Market cut = new Market(1, 0.0, 0.0);

        // Create date of a specific time
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1970);
        cal.set(Calendar.MONTH, 0);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.HOUR_OF_DAY, 13);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        Date currentTime = cal.getTime();

        cut.setHours("13:00 - 15:00");
        assertTrue(cut.isHours(currentTime));
    }

    @Test
    public void isHoursSuccess04() throws Exception {
        Market cut = new Market(1, 0.0, 0.0);

        // Create date of a specific time
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1970);
        cal.set(Calendar.MONTH, 0);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.HOUR_OF_DAY, 8);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        Date currentTime = cal.getTime();

        cut.setHours("7:30 - 8:30");
        assertTrue(cut.isHours(currentTime));
    }

    @Test
    public void isHoursSuccess05() throws Exception {
        Market cut = new Market(1, 0.0, 0.0);

        // Create date of a specific time
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1970);
        cal.set(Calendar.MONTH, 0);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.HOUR_OF_DAY, 7);
        cal.set(Calendar.MINUTE, 30);
        cal.set(Calendar.SECOND, 0);
        Date currentTime = cal.getTime();

        cut.setHours("7:30 - 8:30");
        assertTrue(cut.isHours(currentTime));
    }

    @Test
    public void isHoursFailure01() throws Exception {
        Market cut = new Market(1, 0.0, 0.0);

        // Create date of a specific time
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1970);
        cal.set(Calendar.MONTH, 0);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.HOUR_OF_DAY, 12);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        Date currentTime = cal.getTime();

        cut.setHours("13:00 - 15:00");
        assertFalse(cut.isHours(currentTime));
    }

    @Test
    public void isHoursFailure02() throws Exception {
        Market cut = new Market(1, 0.0, 0.0);

        // Create date of a specific time
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1970);
        cal.set(Calendar.MONTH, 0);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.HOUR_OF_DAY, 15);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 1);
        Date currentTime = cal.getTime();

        cut.setHours("13:00 - 15:00");
        assertFalse(cut.isHours(currentTime));
    }

    @Test
    public void isHoursFailure03() throws Exception {
        Market cut = new Market(1, 0.0, 0.0);

        // Create date of a specific time
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1970);
        cal.set(Calendar.MONTH, 0);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.HOUR_OF_DAY, 7);
        cal.set(Calendar.MINUTE, 29);
        cal.set(Calendar.SECOND, 59);
        Date currentTime = cal.getTime();

        cut.setHours("7:30 - 8:30");
        assertFalse(cut.isHours(currentTime));
    }

    @Test
    public void isHoursFailure04() throws Exception {
        Market cut = new Market(1, 0.0, 0.0);

        // Create date of a specific time
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1970);
        cal.set(Calendar.MONTH, 0);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.HOUR_OF_DAY, 8);
        cal.set(Calendar.MINUTE, 30);
        cal.set(Calendar.SECOND, 1);
        Date currentTime = cal.getTime();

        cut.setHours("7:30 - 8:30");
        assertFalse(cut.isHours(currentTime));
    }

    @Test
    public void isHoursFailure05() throws Exception {
        Market cut = new Market(1, 0.0, 0.0);

        // Create date of a specific time
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1970);
        cal.set(Calendar.MONTH, 0);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.HOUR_OF_DAY, 15);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        Date currentTime = cal.getTime();

        cut.setHours("13:00 - 15:00");
        assertFalse(cut.isHours(currentTime));
    }

    @Test
    public void isHoursFailure06() throws Exception {
        Market cut = new Market(1, 0.0, 0.0);

        // Create date of a specific time
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1970);
        cal.set(Calendar.MONTH, 0);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.HOUR_OF_DAY, 8);
        cal.set(Calendar.MINUTE, 30);
        cal.set(Calendar.SECOND, 0);
        Date currentTime = cal.getTime();

        cut.setHours("7:30 - 8:30");
        assertFalse(cut.isHours(currentTime));
    }
}