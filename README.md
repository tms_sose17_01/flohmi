# **Flohmi** #
* * *
## 1 - Konzeptausführung ##
 
### 1.1 - Wofür ist diese App? ###
 
Die FlohmiApp - abgeleitet von Flohmarkt - bietet dem Benutzer Informationen über alle Wochen- und Trödelmärkte in und um Berlin. Die Märkte lassen sich nach Bezirk und Öffnungszeiten filtern. Außerdem wird eine Suche angeboten, um Märkte zu finden. Die genauen Details zu einem Markt, wie Adresse, Öffnungszeiten und Kontakt zum Marktbetreiber werden ebenfalls angeboten. Über eine Karte kann der Benutzer mit seiner Umgebung interagieren.
 
Die App ist zum Einen für Besucher von Märkten gedacht, die auf die Schnelle mal sehen wollen, wann ein Markt in der Nähe geöffnet hat. Zum Anderen soll die App Händlern, die selber auch mal einen Stand eröffnen, eine Übersicht haben über die Märkte in ihrer Umgebung. Aber auch für Händler, die keinen eigenen Stand öffnen, können sehen, welche Märkte im Laufe eines Tages geöffnet haben, um auch mehrere Märkte besuchen zu können.
 
 
 
### 1.2 - Screenshots ###
 
**Karte (Hauptansicht)**

![Screenshot_1499933414.png](https://bitbucket.org/repo/oLLKaza/images/1631061071-Screenshot_1499933414.png) 
 
Die Nutzung der App beginnt mit einem Punkt zur Kennzeichnung des aktuellen Standorts des Benutzers auf einer Karte. Auf der Karte sind alle vorhandenen Märkte sichtbar. Durch den Klick auf einen Markt wird ein Popup mit zusätzlichen Informationen zu diesem aufgerufen. Bei einem Klick auf dieses Popup gelangt man zur Detailansicht. Durch das Ortungssymbol kann man die Karte auf seine aktuelle Position zentrieren. Die Symbole für Navigation und Google Maps öffnen die entsprechenden Apps.
 
 
 
**Menü** 
 
![Screenshot_1499933430.png](https://bitbucket.org/repo/oLLKaza/images/3849908357-Screenshot_1499933430.png)
 
Das Menü gibt dem User die Möglichkeit, auf diversen Ansichten zu wechseln. Geöffnet wird es durch den Klick auf das Menüsymbol in der Toolbar oder durch das Wischen von Links nach Rechts auf dem Bildschirm.
 
 
 
**Detailansicht** 
 
![Screenshot_1499933419.png](https://bitbucket.org/repo/oLLKaza/images/4181730171-Screenshot_1499933419.png)
 
In der Detailansicht kann der Nutzer die genauen Informationen zu einem Markt abrufen. Dazu gehören neben der Adresse und den Öffnungszeiten auch die Kontaktinformationen zum Marktbetreiber, da diese App u. A. auch für Händler gedacht ist. Außerdem kann dieser über den Stern als Favorit markiert werden. Über den Pfeil gelangt man zurück zur Karte.
 
 
 
**Märkte in der Nähe**
 
![Screenshot_1499933424.png](https://bitbucket.org/repo/oLLKaza/images/3072859961-Screenshot_1499933424.png)
 
In dieser Ansicht werden alle Märkte in einem Umkreis von 4km angezeigt. Die Sortierung erfolgt hierbei aufsteigend nach Entfernung.
 
 
**Favoriten**
 
![Screenshot_1499933483.png](https://bitbucket.org/repo/oLLKaza/images/285061037-Screenshot_1499933483.png)
 
Hier sind alle vom Benutzer als Favorit markierten Märkte aufgelistet. Diese werden, wie in der Ansicht “Märkte in der Nähe” aufsteigend nach Entfernung sortiert.
 

**Disclaimer**

![Screenshot_1499933489.png](https://bitbucket.org/repo/oLLKaza/images/3188608999-Screenshot_1499933489.png)

Diese Ansicht enthält allgemeine Informationen zur Haftung und ein Logo zum Verweis auf die Herkunft der Daten.

 
### 1.3 - App-Navigation ###
 
Der Benutzer landet beim Starten der App in der Karte. Hier kann der Benutzer einen Markt auswählen und auch kurze Informationen zu diesem Markt einsehen.
Mit einem Klick auf den Tooltip gelangt der Benutzer zur Detailansicht des jeweiligen Marktes. Hier kann der Benutzer mit einem Klick auf den Zurück-Button wieder zur Kartenansicht gelangen. Mit dem Betätigen des Sternen-Symbols kann ein Markt in die Favoriten hinzugefügt werden.

Zu jeder Zeit kann der Benutzer über den Menübutton oder eine Wischgeste das Menü öffnen. Über dieses gelangt er zur Karte, zur “Märkte in der Nähe” Ansicht, zu seinen Favoriten sowie zum Disclaimer.

Die App kann mit Hilfe des Zurück-Buttons in der Haupt-/Kartenansicht verlassen werden. 

* * *
 
## 2 - Marketing für den Google Play Store ##
 
### 2.1 - Beschreibung ###
 
Wieder mal den monatlichen Trödelmarkt in deiner Nähe verpasst? Die ganze Stadt durchquert, um festzustellen, dass man auf diesem schrecklichen Markt bereits gewesen ist? Damit ist nun Schluss! Durch Flohmi findet ihr immer die aktuellen Wochen- und Trödelmärkte in eurer Umgebung und könnt eure geliebten Lieblinge direkt abspeichern und nie wieder vergessen.
 
### 2.2 - Screenshots ###
 
Zu den Bildern, die im Google Play Store angezeigt werden sollen gehören u. A. auch ein Screenshot der Karten-/Hauptansicht. Um auch Poweruser anzusprechen soll auch ein Screenshot der Favoriten im Store angezeigt werden.

![Screenshot_1499933414.png](https://bitbucket.org/repo/oLLKaza/images/583732837-Screenshot_1499933414.png)
![Screenshot_1499933419.png](https://bitbucket.org/repo/oLLKaza/images/2895855823-Screenshot_1499933419.png)
![Screenshot_1499933424.png](https://bitbucket.org/repo/oLLKaza/images/2153715818-Screenshot_1499933424.png)

* * *

## 3 - Anforderungen ##

* App ist lauffähig
* App verwendet Open Data
* Projekt liegt in Bitbucket Repository
* Projekt ist gezippt, importierbar und kompilierbar
* Alle Texte in strings.xml
* mindestens 3 verschiedene Screens
* Konzept vorhanden
* *mindestens 3 verschiedene Tests*
* Checks für Checkstyle u. Lint
* App verwendet Fragments
* App ist auf allen Devices bedienbar
* App verwendet Netzwerkzugriff


Texte in strings.xml:
es wurde ein Großteil der Strings in die strings.xml ausgelagert
Lediglich ein paar Strings aus Klassen, die keinen Zugriff auf diese haben, enthalten noch vereinzelte Strings

Eigene Fragments:
NearMarketFragment, MarketDetailsFragment, FavoritesFragment, DisclaimerFragment

* * *

## 4 - Zukünftige Planung ##

* Filterung der Märkte nach Gebieten/Bezirken/Orten
* Suche in der Karte implementieren
* Eigenen Dialog (Fragment) für die Standorterkennung anlegen, wo der User den GPS-Sensor oder die mobilen Daten aktivieren kann